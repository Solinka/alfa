package Step_Definitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import Hooks.Hooks;
import Pars.Pars;
import Elements.AlphaPage;
import Elements.MainPage;
import Elements.MarketPage;
import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class MyStepDefinitions
{
    MainPage MainP = new MainPage();
    MarketPage MarkP = new MarketPage(Hooks.driver);
    AlphaPage Alpha = new AlphaPage();
    Pars pars = new Pars();
    public static String Platform;


    @Дано("^Распарсить файл \"([^\"]*)\", отфильтровать значения по возрастанию и по убыванию$")
    public void распарситьФайлОтфильтроватьЗначенияПоВозрастаниюИПоУбыванию(String file) throws Throwable
    {
        pars.ParsAndFilter(file);
    }

    @Дано("^Зайти на страницу \"([^\"]*)\"$")
    public void зайти_на_страницу(String page) throws Throwable
    {
        Hooks.driver.navigate().to(page);

        Platform = page.substring(page.indexOf(':') + 3, page.lastIndexOf('.'));
    }
    @Когда("^Перешли в раздел \"([^\"]*)\"$")
    public void перешлиВРаздел(String page) throws Throwable
    {
        if (Alpha.Alpha(page))
            return;
        else
            MainP.GoToSection(page);
    }

    @И("^Перешли в подраздел \"([^\"]*)\"$")
    public void перешли_в_подраздел(String page) throws Throwable
    {
        if (Alpha.AlpaWork(page))
            return;
        else
            MarkP.GoToSubSection(page);
    }
    @И("^Отфильтровали по \"([^\"]*)\"$")
    public void отфильтровали(String type) throws Throwable
    {
        MarkP.Filter(type);
    }

    @И("^Выбрали производителя \"([^\"]*)\"$")
    public void выбралиПроизводителя(String prizv) throws Throwable
    {
        MarkP.Firma(prizv);
    }

    @И("^Выбрали цену от \"([^\"]*)\" до \"([^\"]*)\"$")
    public void выбралиЦенуОтДо(String from, String to) throws Throwable
    {
        MarkP.Price(from,to);
    }

    @И("^Запомнили \"([^\"]*)\" товар$")
    public void запомнилиТовар(String num) throws Throwable
    {
        MarkP.Save(num);
    }

    @И("^Клик на \"([^\"]*)\" товар$")
    public void кликНаТовар(String num) throws Throwable
    {
        MarkP.Click(num);
    }


    @Тогда("^Увидели страницу с сохраненным именем товара$")
    public void увиделиСтраницуССохраненнымИменемТовара() throws Throwable
    {
        MarkP.Assert();
    }

    @Когда("^Нашли сайт \"([^\"]*)\"$")
    public void нашлиСайт(String name) throws Throwable
    {
        MainP.Search(name);
    }

    @Тогда("^Сохранили текст в файл$")
    public void сохранилиТекстВФайл() throws Throwable
    {
        Alpha.GetText();
        Alpha.WriteText();
    }

    @И("^Проверили загрузку страницы \"([^\"]*)\"$")
    public void проверилиЗагрузкуСтраницы(String url) throws Throwable
    {
        Alpha.valid(url);
    }
}
