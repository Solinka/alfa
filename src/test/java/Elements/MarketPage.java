package Elements;

import Hooks.Hooks;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MarketPage
{
    static public String namezs = "";
    WebDriver driver;
    public MarketPage(WebDriver driver)
    {
        driver = this.driver;
        PageFactory.initElements(driver, this);
    }

    // Показать всех производителей
//    @FindBy(xpath = "//a[contains(@data-reactid, '152')]")
//    public WebElement all;

    // Поле поиска производителя
//    @FindBy(xpath = "//input[contains(@id, '7893318-suggester')]")
//    public WebElement search;

    public void GoToSubSection(String name) throws InterruptedException {
        Hooks.driver.findElement(By.xpath("//a[contains(@class, 'topmenu__link') and text() = '"+name+"']")).click();
    }
    public void Filter(String name) throws InterruptedException
    {
//        Hooks.waitClickable(Hooks.driver.findElement(By.xpath("//a[contains(@class, 'catalog-menu__list-item') and text() = '"+name+"']")), 15);
        if (name.contains("Наушники и Bluetooth-гарнитуры"))
            Hooks.driver.findElement(By.xpath("(//a[contains(@class, 'catalog-menu__list-item') and text() = '"+name+"'])[2]")).click(); // :(
        else Hooks.driver.findElement(By.xpath("//a[contains(@class, 'catalog-menu__list-item') and text() = '"+name+"']")).click();
    }
    public void Firma(String prod) throws InterruptedException
    {
        Hooks.waitClickable(Hooks.driver.findElement(By.xpath("//a[contains(@data-reactid, '152')]")), 15);
        Hooks.waitVisible(Hooks.driver.findElement(By.xpath("//a[contains(@data-reactid, '152')]")), 15);
        Hooks.Element("//a[contains(@data-reactid, '152')]").click();
        Hooks.waitClickable(Hooks.driver.findElement(By.xpath("//input[contains(@id, '7893318-suggester')]")), 15);
        Hooks.Element("//input[contains(@id, '7893318-suggester')]").sendKeys(prod);
        Hooks.waitClickable(Hooks.driver.findElement(By.xpath("//div[contains(@class,'_16hsbhrgAf')]//span[text() = '"+prod+"']/..")),10);
        Hooks.Element("//div[contains(@class,'_16hsbhrgAf')]//span[text() = '"+prod+"']/..").click();

    }
    public void Price(String from, String to)
    {
        Hooks.Element("//input[contains(@type, 'text') and contains(@class, 'Wht')]").sendKeys(from);
        Hooks.Element("//input[contains(@type, 'text') and contains(@class, 'As')]").sendKeys(to);
    }
    public void Save(String num)
    {
        Hooks.waitVisible(Hooks.Element("(//div[contains(@class, 'n-snippet-cell2__header')]//a[contains(@class, 'n-link_theme_blue')])["+num+"]"), 15);
        namezs = Hooks.Element("(//div[contains(@class, 'n-snippet-cell2__header')]//a[contains(@class, 'n-link_theme_blue')])["+num+"]").getText();
    }
    public void Click(String num)
    {
        Hooks.waitVisible(Hooks.Element("(//div[contains(@class, 'n-snippet-cell2__header')]//a[contains(@class, 'n-link_theme_blue')])["+num+"]"), 20);
        Hooks.Element("(//div[contains(@class, 'n-snippet-cell2__header')]//a[contains(@class, 'n-link_theme_blue')])["+num+"]").click();
    }
    public void Assert()
    {
        Hooks.waitVisible(Hooks.Element("//h1"), 15);
        Assert.assertEquals("Не совпадает с элементом, котрый запомили", namezs, Hooks.Element("//h1").getText());
    }
}
