package Elements;

import Hooks.Hooks;
import org.junit.Assert;
import org.openqa.selenium.By;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import java.util.ArrayList;

public class MainPage
{
    public void GoToSection(String name) throws InterruptedException
    {
        Hooks.Element("//div[contains(@class, 'home-arrow__tabs')]//a[text() = '"+name+"']").click();
    }

    public void Search(String name) throws InterruptedException
    {
        Hooks.waitVisible(Hooks.Element("//input[contains(@class, 'input__input')]"), 10);
        Hooks.Element("//input[contains(@class, 'input__input')]").sendKeys(name);  //Заполняет строку поиска
        Hooks.Element("//button[contains(@class, 'button_js_inited')]").click();    //Клик по Найти
        Hooks.waitVisible(Hooks.Element("//div[contains(text(), 'Информация об услугах')]/../..//h2"),20);
        String oldWindows =  Hooks.driver.getWindowHandle();
        Hooks.Element("//div[contains(text(), 'Информация об услугах')]/../..//h2").click();//Клик по нужной ссылке Альфа-Банка
        for(String winHandle : Hooks.driver.getWindowHandles())
        {
            if (!winHandle.equals(oldWindows))
                Hooks.driver.switchTo().window(winHandle);
        }
        Hooks.Element("//div[contains(@class, 'sections__3U7en')]");
//        ArrayList<String> tabs2 = new ArrayList<String> (Hooks.driver.getWindowHandles());
//        Hooks.driver.switchTo().window(tabs2.get(1));
//        Hooks.waitVisible(Hooks.Element("//div[contains(@class, 'sections__3U7en')]"),20);
//        Hooks.waitClickable(Hooks.Element("//div[contains(@class, 'sections__3U7en')]"),20);
//        Assert.assertTrue("Страница не отобразилась", Hooks.driver.getCurrentUrl().contains("http://job.alfabank.ru)"));
    }
}
