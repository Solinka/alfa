package Elements;

import Hooks.Hooks;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import Step_Definitions.*;
import org.junit.Assert;

public class AlphaPage
{
    static String txt = "";
    static String path;
    public boolean Alpha(String name)
    {
        Hooks.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        try {
            Hooks.Element("//a[text() = '" + name + "']").click(); //Кликает по "Вакансии"
            Hooks.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return true;
        }
        catch (Exception e)
        {
            Hooks.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return false;
        }
    }
    public boolean AlpaWork(String name)
    {
        Hooks.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        try
        {
            Hooks.Element("//span[contains(@class, 'nav_item-link-helper') and text() = '" + name + "']").click();
            Hooks.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return  true;
        }
        catch (Exception e)
        {
            Hooks.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return false;
        }
    }
    public void GetText()
    {
        txt = Hooks.Element("//div[@class = 'message']").getText() + " " +
                Hooks.Element("//div[@class = 'info']").getText();
    }
    public void valid(String name)
    {
        Hooks.waitVisible(Hooks.Element("//h1"), 10);
        Assert.assertTrue("Страница не отобразилась", Hooks.driver.getCurrentUrl().contains(name));
    }
    public void WriteText() throws IOException, AWTException
    {
        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd_mm_yyyy hh_mm_ss ");
        path = "res//" + formatForDateNow.format(dateNow) + "Google_Chrome " + MyStepDefinitions.Platform + ".txt";

        FileWriter writer = new FileWriter(path, false);
        writer.write(txt);
        writer.flush();
    }
}
