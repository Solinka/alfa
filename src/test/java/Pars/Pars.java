package Pars;
import javax.swing.*;
import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.io.PrintWriter;

public class Pars {
    public static Scanner scn;
    public static Integer[] m = new Integer[20];
    public static String string = "";

    public void ParsAndFilter (String  file1)
    {
        OpenFiles(file1);
        ReadFiles();
        Out();
    }

    public static void OpenFiles (String name) {
        try {
            scn = new Scanner(new File("res//"+name+"")).useDelimiter(",");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Файл не найден");
        }
    }

    public static void ReadFiles(){
        while (scn.hasNext()){
            for (int i = 0; i < m.length; i++)
            {
                m[i] = Integer.parseInt(scn.next());
                string = string + m[i].toString();
            }
        }
    }

    public static void Out()
    {
        Arrays.sort(m);
        for (int i = 0; i < m.length; i++)
            System.out.print(m[i] + "  ");
        System.out.println();
        Arrays.sort(m, Collections.reverseOrder());
        for (int i = 0; i < m.length; i++)
            System.out.print(m[i] + "  ");
    }



//    public  void Writef(String fileName, String text)
//    {
//        File file = new File("//res" + fileName);
//
//        try {
//            if(!file.exists()){
//                file.createNewFile();
//            }
//
//            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
//
//            try {
//                out.print(text);
//            } finally {
//                //закрыть файл, чтобы записался
//                out.close();
//            }
//        } catch(IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
}
