package Hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import javax.swing.*;

public class Hooks
 {
    public static WebDriver driver;
    public static String currentPage = "";
    @Before
    public void startBrowser()
    {
        System.setProperty("webdriver.chrome.driver", "./webdrivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void closeBrowser()
    {
        driver.quit();
    }


     public static WebElement Element(String XPath)
     {
         return driver.findElement(By.xpath(XPath));
     }

     public static void waitVisible(WebElement element, int timeout) {
         WebDriverWait wait = new WebDriverWait(Hooks.driver,timeout);
         wait.until(ExpectedConditions.visibilityOf(element));
     }
     public static void waitClickable(WebElement element, int timeout) {
         WebDriverWait wait = new WebDriverWait(Hooks.driver, timeout);
         wait.until(ExpectedConditions.elementToBeClickable(element));
     }
     public static void waitinvis(String locator, int timeout) {
         WebDriverWait wait = new WebDriverWait(Hooks.driver,timeout);
         wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
     }
}
